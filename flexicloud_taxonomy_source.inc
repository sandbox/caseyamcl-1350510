<?php

function flexicloud_add_flexicloud_source() {

  $sources = array();
  
  $sources['taxonomy'] = array(
    'title' => t('Taxonomy Flexicloud Source'),
    'description' => t('Build tag clouds from taxonomy terms'),
    'tags_callback' => 'flexicloud_taxonomy_get_tags',
    'settings_callback' => 'flexicloud_taxonomy_get_settings',
    'links_callback' => 'flexicloud_taxonomy_get_link'
  );

  return $sources;
}

//------------------------------------------------------------------------

function flexicloud_taxonomy_get_link($tag_term) {

  $tax_item = taxonomy_get_term_by_name($tag_term);

  if ($tax_item) {
    $tax_item = array_shift($tax_item);
    return 'taxonomy/term/'. $tax_item->tid;
  }   
  else
    return FALSE;

}

//------------------------------------------------------------------------

function flexicloud_taxonomy_get_tags($settings = array()) {
  
  $tags_from_each_tax = array();
  
  foreach($settings[0]['taxonomy_name'] as $tax_id) {
    
    foreach(taxonomy_get_tree($tax_id) as $tax_term) {
      $tags_from_each_tax[$tax_id][$tax_term->name] = count(taxonomy_select_nodes($tax_term->tid, FALSE, FALSE));
    }
  }
 
  //@TODO: Fix this so that it reports the correct post counts
  //@TODO: Fix this so it eliminates duplicates
  $tags = array();
  foreach($tags_from_each_tax as $tagset) {
    $tags = array_merge($tags, $tagset);
  }
  
  return $tags;  
}

//------------------------------------------------------------------------

function flexicloud_taxonomy_get_settings() {
  
  //Get the taxonomies
  $vocabs = array();
    
  foreach(taxonomy_get_vocabularies() as $v) {
    $vocabs[$v->vid] = $v->name;
  }
  
  $form_fields = array();

  if (count($vocabs) > 0) {
  
    $form_fields['taxonomy_name'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Taxonomy Source'),
      '#options' => $vocabs,
      '#description' => t('Select one or more taxonomies to use for the tag cloud')
    );    
    
  }
  else {
    
    $form_fields['taxonomy_msg'] = array(
      '#markup' => t('No vocabularies available')
    );
    
  }
  
  return $form_fields;
}

/* EOF: flexicloud_taxonomy_source.php */