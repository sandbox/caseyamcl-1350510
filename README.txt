Flexicloud - A Drupal 7 Module for creating Flexible Tag Clouds
===============================================================

Flexicloud allows you to create flexible tag clouds from any source (not just
from taxonomies).

v1 DEVELOPMENT TODO
===================
* Figure out best algorithm for terms from flexibiblio (incorporate that logic into the base module)
* Tag link patterns should be different depending on the source (taxonomy should like to a URL structure, biblio another, etc)
* Form validation and validation for hooked forms
* Custom class per cloud name on output list
* Need to finish delete for taxonomy entities in the database (how to preload form values manually?)
* Output algorithms should be options.  Book on algorithms: http://shop.oreilly.com/product/9780596527945.do?sortby=bestSellers
* Document API hooks for other module developers in Doxygen

FUTURE DEVELOPMENT
==================
 * De-drupalify the nuts and bolts into a general purpose library (?) - This would allow me to
 * - Publish this as a general purpose tool on my site
 * - Build plugins for CodeIgniter, etc
 * - Provide an API
 * - Turn basic functionalities into classes, and then latch Drupal on top of them