<?php

/**
 * Generate management page for flexiclouds
 * 
 * @return string
 * HTML output
 */
function flexicloud_manage() {
  
  //Get all of the flexicloud ids
  $ids = array();
  foreach(db_query("SELECT fxid FROM {flexicloud}") as $row) {
    $ids[] = $row->fxid;
  }
    
  //Setup table rows from the clouds
  $manage_table_rows = array();
  foreach(flexicloud_load_multiple($ids) as $cloud) {
    $row = array();
    $row[] = $cloud->settings['cloud_name'];
    $row[] = l('Preview Cloud', 'cloud/' . $cloud->fxid);
    $row[] = l('Edit Cloud', 'admin/structure/flexicloud/update/' . $cloud->fxid);
    $row[] = l('Delete Cloud', 'admin/structure/flexicloud/delete/' . $cloud->fxid);
    $manage_table_rows[] = $row;
    unset($row);
  }
    
  //Create the table
  $manage_table = array(
    'header' => array('Cloud Name', array('data' => 'Operations', 'colspan' => 3)),
    'rows' => $manage_table_rows,
    'caption' => '',
    'attributes' => array(),
    'colgroups' => array(),
    'sticky' => NULL,
    'empty' => NULL
  );

  $content = array();
  

  $content[] = theme_item_list(array(
    'items' => array(array( 'data' => l('Add Flexicloud', 'admin/structure/flexicloud/create') )),
    'title' => '',
    'type' => 'ul',
    'attributes' => array('class' => 'action-links')
  ));
  
  
  $content[] = theme_table($manage_table);
    
  return (implode("\n",$content));
}

//------------------------------------------------------------------------

/**
 * Flexicloud create/update management page
 * 
 * @param string $action
 * @param int $record
 * @return string
 * HTML output 
 */
function flexicloud_manage_crud($action = 'create', $record = NULL) {
    
  if ($action == 'update') {
    $rec = flexicloud_load($record);
    
    if ( ! $rec) {
      drupal_set_message(t('You must specify a valid record to update!'));
      drupal_goto('admin/structure/flexicloud');
    }    
  }
  else { /* New Record */
    $rec = array();
  }

  return drupal_get_form('flexicloud_crud_form', $rec);
}

//------------------------------------------------------------------------

function flexicloud_manage_delete($fxid) {
  
  return drupal_get_form('flexicloud_delete_form', $fxid);
}

//------------------------------------------------------------------------

function flexicloud_delete_form_submit($form, &$form_state) {
  
  $id = $form_state['values']['confirmation'];
  
  //Delete the entity
  flexicloud_delete($id);
  
  drupal_set_message(check_plain(t("Cloud with ID @id Deleted", array('@id' => $id))));
  drupal_goto('admin/structure/flexicloud');
  
}

//------------------------------------------------------------------------

function flexicloud_delete_form_validate($form, &$form_state) {
  
  //@TODO: Permissions check (?)
  
  $id = array_pop(explode('/', current_path()));
  
  if ($form_state['values']['confirmation'] != $id)
    form_set_error('confirmation', t("You must check the box to confirm deletion!"));
  
}

//------------------------------------------------------------------------

function flexicloud_delete_form($form, &$form_state, $fxid) {
  
  $form = array();
   
  $form['msg_head'] = array(
    '#markup' => "<h3>Are you sure?</h3><p>Once you delete a cloud, there is no going back!</p>"
  );
  
  $form['confirmation'] = array(
    '#type' => 'checkbox',
    '#title' => t("Check here to confirm deletion of Flexicloud #@fxid", array('@fxid' => $fxid)),
    '#return_value' => $fxid
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm Delete')
  );
  
  return $form;
  
}

//------------------------------------------------------------------------

/**
 * Flexicloud CRUD form submit method
 * 
 * @param array $form
 * @param array $form_state 
 */
function flexicloud_crud_form_submit($form, &$form_state) {

  //Process the basic form data fields here
  $record = array(
    'cloud_name'            => $form_state['values']['cloud_name'],
    'sources'               => $form_state['values']['sources'],
    'always_exclude'        => $form_state['values']['always_exclude'],
    'use_default_stopwords' => $form_state['values']['use_default_stopwords']
  );
  
  $record['display_settings'] = array(
    'max_tags'       => $form_state['values']['max_tags'],
    'disp_order'     => $form_state['values']['disp_order'],
    'include_style'  => $form_state['values']['include_style'],      
    'weight_range'   => $form_state['values']['weight_range']
  );
  
  //Process the additional source data fields here
  
  if (flexicloud_get_datasources('settings_callback', $record['sources'])) {

    $ds_fields = call_user_func(flexicloud_get_datasources('settings_callback', $record['sources']));
    $ds_fields = flexicloud_settings_get_setting_names($ds_fields);
    
    foreach($form_state['values'] as $k => $v) {
      
      if (in_array($k, $ds_fields)) {
        $record['source_settings'][$k] = $form_state['values'][$k];
      }
      
    }    
  }
     
  //Setup the record and primary key
  $record = array('settings' => json_encode($record));
  
  //Updating?
  if (isset($form_state['values']['fxid'])) {
    $record['fxid'] = $form_state['values']['fxid'];
    $pk = 'fxid';
    $msg = t("Updated the flexicloud!");
  }
  else {
    $pk = array();
    $msg = t("Created the flexicloud!");
  }
  
  //Write it out
  drupal_write_record('flexicloud', $record, $pk);
  module_invoke_all('entity_insert', 'flexicloud', $record);
  drupal_set_message(check_plain($msg));
  drupal_goto('admin/structure/flexicloud');
}

//------------------------------------------------------------------------

/**
 * Return the main form for flexicloud create/update
 * 
 * @param array $form
 * @param array $form_state
 * @param array $form_data
 * @return array
 */
function flexicloud_crud_form($form, &$form_state, $form_data = array()) {

  drupal_add_css(drupal_get_path('module', 'flexicloud') . '/cloud_manage.css');

  $record_info = (isset($form_data->settings)) ? $form_data->settings : array();
  //dvm($record_info);
  
  $form = array();
    
  //If updating...
  if (isset($form_data->fxid)) {
    $form['fxid'] = array(
      '#type' => 'hidden',
      '#value' => $form_data->fxid
    );
  }
  
  $form['cloud_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Cloud Name'),
    '#description' => t('Provide a name for your cloud'),
    '#required' => TRUE,
    '#default_value' => (isset($record_info['cloud_name'])) ? $record_info['cloud_name'] : ''
  );
  
  $form['sources'] = array(
    '#type' => 'select',
    '#title' => t('Datasource type'),
    '#options' => flexicloud_get_datasources('title'),
    '#empty_option' => ' - ' . t('Select a source') . ' - ',
    '#required' => TRUE,  
    '#default_value' => (isset($record_info['sources'])) ? $record_info['sources'] : ''
  );
    
  foreach(flexicloud_get_datasources('settings_callback') as $sourcename => $settings_callback) {
    
    $form["source_settings_$sourcename"] = array(
      '#type' => 'fieldset',
      '#title' => flexicloud_get_datasources('title', $sourcename) . " Settings",
      '#states' => array(
        'visible' => array('select[name=sources]' => array('value' => $sourcename))
     )
        
    );
            
    $sform = call_user_func($settings_callback);
    
    foreach($sform as $k => $v) {
      $form["source_settings_$sourcename"][$k] = $v;
      
      //Overwrite the default value if we are updating...
      if (isset($record_info['source_settings'][$k])) {
        $form["source_settings_$sourcename"][$k]['#default_value'] = $record_info['source_settings'][$k];
      }
      
    }
  }

  $form['always_exclude'] = array(
    '#type' => 'textfield',
    '#title' => t('Always Exclude Terms (stop words)'),
    '#description' => t('Enter terms to always exclude, separated by commas'),
    '#required' => FALSE,
    '#default_value' => (isset($record_info['always_exclude'])) ? $record_info['always_exclude'] : ''
  );

  $form['use_default_stopwords'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Default Stop Words (recommended for English sites only)'),
    '#description'=> "<span class='stopword_toggle'>" . t('View Stopwords') . "</span>",
    '#required' => FALSE,
    '#default_value' => (isset($record_info['use_default_stopwords'])) ? $record_info['use_default_stopwords'] : TRUE
  );
  
  $form['display_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cloud Display Settings')
  );
  
  $form['display_settings']['max_tags'] = array(
    '#type' => 'textfield',
    '#title' => t('Max Number of Tags.  0 for unlimited'),
    '#size' => '15',
    '#maxlength' => '5',
    '#required' => TRUE,
    '#default_value' => (isset($record_info['display_settings']['max_tags'])) ? $record_info['display_settings']['max_tags'] : '50'
  );
  
  $disp_orders = array(
    'abc_asc' => t('Alphabetical (ascending)'),
    'abc_desc' => t('Alphabetical (decending)'),
    'random' => t('Random Order'),
    'weight_asc' => t('By weight (ascending)'),
    'weight_desc' => t('By weight (descending)')
  );
  
  $form['display_settings']['disp_order'] = array(
    '#type' => 'radios',
    '#title' => t('Display Order'),
    '#options' => $disp_orders,
    '#required' => TRUE,
    '#default_value' => (isset($record_info['display_settings']['disp_order'])) ? $record_info['display_settings']['disp_order'] : 'random'
  );

  $form['display_settings']['include_style'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include builtin stylesheets'),
    '#description' => t('Uncheck if you wish to apply your own stylesheet to the output'),
    '#default_value' => (isset($record_info['display_settings']['include_style'])) ? $record_info['display_settings']['include_style'] : 1
  );

  $form['display_settings']['weight_range'] = array(
    '#type' => 'textfield',
    '#title' => t('Weight range (min: 5, max: 20)'),
    '#description' => t("The range dictates the number of different tag weights assigned to a cloud"),
    '#size' => '12',
    '#maxlength' => '5',
    '#required' => TRUE,
    '#default_value' => (isset($record_info['display_settings']['weight_range'])) ? $record_info['display_settings']['weight_range'] : 15
 );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => (isset($form_data->fxid)) ? t('Update flexicloud!') :t('Create flexicloud!')
  );
  
  return $form;
}

//------------------------------------------------------------------------

/**
 * Helper function to flatten form items into a list of settings names
 * 
 * @TODO: This is probably not foolproof logic.
 * @param array $settings
 * @return array
 */
function flexicloud_settings_get_setting_names($settings) {
    
  $out_array = array();
  
  foreach($settings as $k => $v) {
    
    if (substr($k, 0, 1) !== "#") {
      $out_array[] = $k;
    }
    
    if (is_array($v) && $k != '#states') {
      $out_array = array_merge($out_array, flexicloud_settings_get_setting_names($v));
    }
    
  }
  
  return $out_array;
}


/* EOF: flexicloud_admin.php */